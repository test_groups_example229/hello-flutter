import 'package:flutter/material.dart';

enum APP_THEME{LIGHT, DARK}


void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.blueGrey,
      ),
    );
  }
  static ThemeData appThemeDark() {
    return ThemeData (
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.blueGrey,
      ),
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK ? MyAppTheme.appThemeLight() : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.shield_moon_rounded),
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          //color: Colors.blueGrey,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          //color: Colors.blueGrey,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          //color: Colors.blueGrey,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          //color: Colors.blueGrey,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          //color: Colors.blueGrey,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          //color: Colors.blueGrey,
        ),
        onPressed: () {},
      ),
      Text("Pay"),

    ],
  );
}

//ListTile
Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("330-803-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton (
      icon: Icon(Icons.message),
      color: Colors.blueGrey,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading: Text(""),
    title: Text("440-440-3390"),
    subtitle: Text("other"),
    trailing: IconButton (
      icon: Icon(Icons.message),
      color: Colors.blueGrey,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("sarocha@gmail.com"),
    subtitle: Text("work"),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("234 Sunset St, Burlingame"),
    subtitle: Text("home"),
    trailing: IconButton (
      icon: Icon(Icons.directions),
      color: Colors.blueGrey,
      onPressed: () {},
    ),
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildDirectionsButton(),
      buildEmailButton(),
      buildPayButton(),
      buildVideoCallButton()
    ],
  );
}

AppBar buildAppBarWidget() {
  return AppBar (
    backgroundColor: Colors.pink,

    leading: Icon(
      Icons.arrow_back,
      color: Colors.white,
    ),
    actions: <Widget>[
      IconButton(onPressed: () {}, icon: Icon(Icons.star_border),
          color: Colors.white
      )
    ],
  );
}

Widget buildBodyWidget() {
  return ListView (
    children: <Widget>[
      Column (
        children: <Widget>[
          Container(
            color: Colors.blueGrey,
            child: Image.network(
              "https://i1.wp.com/www.somjook.com/wp-content/uploads/2021/04/e54c060156c18cc776b4e15da2d6d035.jpg?resize=236%2C236&ssl=1",
              fit: BoxFit.cover,
              width: double.infinity,
              height: 250,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(padding: EdgeInsets.all(8.0),
                  child: Text("Kaopod",
                    style: TextStyle(fontSize:30),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.black26,
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child : Theme(
              data: ThemeData(
                  iconTheme: IconThemeData(
                    color: Colors.pink,
                  )
              ),
              child: profileActionItems(),
            ),
          ),
          mobilePhoneListTile(),
          otherPhoneListTile(),
          emailListTile(),
          addressListTile(),
        ],
      ),
    ],
  );
}

